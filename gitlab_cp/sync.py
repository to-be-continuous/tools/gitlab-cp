import argparse
import json
import os
import shutil
from enum import Enum
from logging import Logger
from pathlib import Path
from typing import Optional, Union

import requests
from git import GitError, Repo
from gitlab import (
    Gitlab,
    GitlabCreateError,
    GitlabDeleteError,
    GitlabGetError,
    GitlabListError,
    GitlabUpdateError,
)
from gitlab.v4.objects import Group, Project, ProjectCommit

LOGGER = Logger(__name__)


class AnsiColors:
    BLACK = "\033[0;30m"
    RED = "\033[0;31m"
    GREEN = "\033[0;32m"
    YELLOW = "\033[0;33m"
    BLUE = "\033[0;34m"
    PURPLE = "\033[0;35m"
    CYAN = "\033[0;36m"
    WHITE = "\033[0;37m"

    HGRAY = "\033[90m"
    HRED = "\033[91m"
    HGREEN = "\033[92m"
    HYELLOW = "\033[93m"
    HBLUE = "\033[94m"
    HPURPLE = "\033[95m"
    HCYAN = "\033[96m"
    HWHITE = "\033[97m"

    RESET = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"


class GlVisibility(str, Enum):
    """GitLab project visibility."""

    public = "public"
    internal = "internal"
    private = "private"

    def __str__(self) -> str:
        return self.name


class Synchronizer:
    def __init__(
        self,
        src_client: Gitlab,
        src_sync_path: str,
        dest_client: Optional[Gitlab],
        dest_sync_path: str,
        work_dir: Path,
        max_visibility=GlVisibility.public,
        skip_visibility=False,
        exclude: Optional[list[str]] = None,
        include: Optional[list[str]] = None,
        update_release=False,
        group_description=True,
        project_description=True,
        dry_run=True,
        continue_on_error=False,
        update_avatar=False,
        ssl_verify=True,
        new_project_options={},
        new_group_options={},
    ):
        self.src_client = src_client
        self.src_sync_path = src_sync_path
        self.dest_client = dest_client
        self.dest_sync_path = dest_sync_path
        self.work_dir = work_dir
        self.max_visibility = max_visibility
        self.skip_visibility = skip_visibility
        self.exclude = exclude or []
        self.include = include or []
        self.force_update_latest_release = update_release
        self.group_description = group_description
        self.project_description = project_description
        self.dry_run = dry_run
        self.continue_on_error = continue_on_error
        self.force_update_avatar = update_avatar
        self.ssl_verify = ssl_verify
        self.errors: list[Exception] = []
        self.warnings: list[Exception] = []
        self.groups_count = 0
        self.projects_count = 0
        self.new_project_options = new_project_options
        self.new_group_options = new_group_options

    def handle_error(self, err: Exception) -> None:
        self.errors.append(err)
        if not self.continue_on_error:
            raise err

    def handle_warn(self, err: Exception) -> None:
        self.warnings.append(err)

    def adjust_visibility(self, visi: GlVisibility) -> str:
        if self.max_visibility == GlVisibility.public:
            return visi
        if self.max_visibility == GlVisibility.internal:
            return GlVisibility.internal if visi == GlVisibility.public else visi
        return "private"

    def rel_path(self, src_path: str) -> str:
        assert src_path.startswith(self.src_sync_path)
        return (
            ""
            if src_path == self.src_sync_path
            else src_path[len(self.src_sync_path) + 1 :]
        )

    def is_excluded(self, src_path: str) -> bool:
        return self.rel_path(src_path) in self.exclude

    def is_included(self, src_path: str) -> bool:
        # true if:
        # - there's no include list (any group/project is included by default)
        # - realtive path exactly matches an include item
        # - realtive path is an ascendant of an included item (parent groups must be considered to reach the item)
        # - realtive path is a descendant of an included item (inclusion of a group is recursive)
        def are_related_paths(p1, p2):
            return (p1 == p2) or p2.startswith(p1 + "/") or p1.startswith(p2 + "/")
        rel_path = self.rel_path(src_path)
        return (not self.include) \
            or True in (are_related_paths(rel_path, x) for x in self.include)

    def to_dest_path(self, src_path: str) -> str:
        return (
            self.dest_sync_path
            if src_path == self.src_sync_path
            else self.dest_sync_path + "/" + self.rel_path(src_path)
        )

    # Synchronize a project release
    def sync_releases(
        self,
        src_project: Project,
        dest_project: Project,
    ) -> None:
        src_releases = src_project.releases.list(all=True)
        print(f"    - sync {len(src_releases)} releases...")
        dest_releases = dest_project.releases.list(all=True)
        for idx, src_release in enumerate(src_releases):
            tag_name: str = src_release.tag_name
            dest_release = next(
                filter(
                    lambda rel: rel.tag_name == tag_name,
                    dest_releases,
                ),
                None,
            )
            tag_name = src_release.tag_name
            if dest_release is None:
                # dest release does not exist: create
                if self.dry_run:
                    print(
                        f"      - {tag_name}: {AnsiColors.HGREEN}create needed{AnsiColors.RESET}"
                    )
                else:
                    try:
                        dest_project.releases.create(
                            {
                                "tag_name": tag_name,
                                "name": tag_name,
                                "released_at": src_release.released_at,
                                "description": src_release.description,
                            }
                        )
                        print(
                            f"      - {tag_name}: {AnsiColors.HGREEN}created{AnsiColors.RESET}"
                        )
                    except GitlabCreateError as ge:
                        print(
                            f"      - {tag_name}: create {AnsiColors.HRED}failed{AnsiColors.RESET}",
                            ge,
                        )
                        self.handle_error(ge)
            elif idx == 0 and self.force_update_latest_release:
                if self.dry_run:
                    print(
                        f"      - {tag_name}: {AnsiColors.HYELLOW}update needed{AnsiColors.RESET} (force)"
                    )
                else:
                    # dest release already exists: update
                    try:
                        dest_release.name = tag_name
                        dest_release.released_at = src_release.released_at
                        dest_release.description = src_release.description
                        dest_release.save()
                        print(
                            f"      - {tag_name}: {AnsiColors.HYELLOW}updated{AnsiColors.RESET} (force)"
                        )
                    except GitlabUpdateError as ge:
                        print(
                            f"      - {tag_name}: update {AnsiColors.HRED}failed{AnsiColors.RESET}",
                            ge,
                        )
                        self.handle_error(ge)
            else:
                # dest release already exists: skip
                print(
                    f"      - {tag_name}: {AnsiColors.HGRAY}up-to-date{AnsiColors.RESET}"
                )

    def sync_git_repo(self, src_project: Project, dest_project: Project) -> None:
        src_default_branch = src_project.default_branch
        try:
            src_latest_commit: ProjectCommit = next(
                src_project.commits.list(ref_name=src_default_branch, iterator=True),
                None,
            )
        except GitlabListError as ge:
            print(
                f"    - latest commit from source repo: get {AnsiColors.HRED}failed{AnsiColors.RESET}",
                ge,
            )
            self.handle_error(ge)
            return

        try:
            dest_latest_commit = next(
                dest_project.commits.list(ref_name=src_default_branch, iterator=True),
                None,
            )
        except GitlabListError as ge:
            print(
                f"    - latest commit from dest repo: get {AnsiColors.HRED}failed{AnsiColors.RESET}",
                ge,
            )
            self.handle_error(ge)

        if ( src_latest_commit is None ):
            # Git sync is not required: skip
            print(
                f"    - git repository: {AnsiColors.HGRAY}empty{AnsiColors.RESET}"
            )
            return
        
        if (
            dest_latest_commit
            and src_latest_commit.get_id() == dest_latest_commit.get_id()
        ):
            # Git sync is not required: skip
            print(
                f"    - git repository: {AnsiColors.HGRAY}up-to-date{AnsiColors.RESET} ({src_default_branch} on #{src_latest_commit.get_id()})"
            )
            return

        # Git sync is required
        repo_dir = self.work_dir / self.rel_path(src_project.path_with_namespace)
        shutil.rmtree(path=repo_dir, ignore_errors=True)

        src_repo_url: str = src_project.http_url_to_repo
        if self.src_client.private_token:
            # insert login/password in Git https url
            src_repo_url = src_repo_url.replace(
                "://", f"://token:{self.src_client.private_token}@"
            )

        # git clone --bare "$src_repo_url" "$repo_name"
        try:
            repo = Repo.clone_from(
                url=src_repo_url,
                to_path=repo_dir,
                bare=True,
                allow_unsafe_options=True,
                env={} if self.src_client.ssl_verify else {"GIT_SSL_NO_VERIFY": "1"},
            )
        except GitError as ge:
            print(
                f"    - source repo: git clone {AnsiColors.HRED}failed{AnsiColors.RESET}",
                ge,
            )
            self.handle_error(ge)
            return

        if self.dry_run:
            print(
                f"    - git repository: {AnsiColors.HYELLOW}update needed{AnsiColors.RESET} ({src_default_branch} on #{src_latest_commit.get_id()})"
            )
            return

        # if project already exists: unprotect default branch first
        if dest_latest_commit:
            try:
                dest_project.protectedbranches.delete(src_default_branch)
            except GitlabDeleteError as ge:
                if ge.response_code != 404:
                    print(
                        f"    - unprotect default branch ({src_default_branch}) in dest project: {AnsiColors.HRED}failed{AnsiColors.RESET}",
                        ge,
                    )
                    self.handle_error(ge)

        dest_repo_url = (
            dest_project.http_url_to_repo
            if dest_project
            else f"https://gitlab.dummy/{src_project.path_with_namespace}.git"
        )
        if self.dest_client.private_token:
            # insert login/password in Git https url
            # shellcheck disable=SC2001
            dest_repo_url = dest_repo_url.replace(
                "://", f"://token:{self.dest_client.private_token}@"
            )

        # git push --force "$dest_repo_url" --tags "$src_default_branch"
        try:
            dest = repo.create_remote("dest", dest_repo_url)
            dest.push(
                refspec=src_default_branch,
                force=True,
                tags=True,
                allow_unsafe_options=True,
                env=({} if self.dest_client.ssl_verify else {"GIT_SSL_NO_VERIFY": "1"}),
            ).raise_if_error()
        except GitError as ge:
            print(
                f"    - dest repo: git push --force {AnsiColors.HRED}failed{AnsiColors.RESET}",
                ge,
            )
            self.handle_error(ge)

        # protect default branch
        try:
            dest_project.protectedbranches.create({"name": src_default_branch})
        except GitlabCreateError as ge:
            if ge.response_code != 409 and ge.response_code != 422:
                print(
                    f"    - protect default branch ({src_default_branch}) in dest project: {AnsiColors.HRED}failed{AnsiColors.RESET}",
                    ge,
                )
                self.handle_error(ge)

        print(
            f"    - git repository: {AnsiColors.HYELLOW}updated{AnsiColors.RESET} ({src_default_branch} on #{src_latest_commit.get_id() if src_latest_commit else 'n/a'})"
        )

    # Synchronize a project/group avatar
    def sync_avatar(
        self,
        src_avatar: Project | Group,
        dest_avatar: Project | Group,
    ) -> None:
        assert all(any(isinstance(resource, type) for type in [Project, Group]) for resource in [src_avatar, dest_avatar])
        
        # Python lib `python-gitlab` doesn't support avatar endpoint (02/12/2024):
        # > We write our own requests calls.      
        src_web_url = src_avatar.web_url
        src_avatar_url = src_avatar.avatar_url
        src_avatar_resource = "projects" if isinstance(src_avatar, Project) else "groups"
        src_avatar_api = f"{self.src_client.api_url}/{src_avatar_resource}/{src_avatar.id}/avatar"
        src_avatar_api_headers = {"PRIVATE-TOKEN": self.src_client.private_token} if self.src_client.private_token is not None else {}
        dest_web_url = dest_avatar.web_url
        dest_avatar_url = dest_avatar.avatar_url
        dest_avatar_resource = "projects" if isinstance(dest_avatar, Project) else "groups"
        dest_avatar_api = f"{self.dest_client.api_url}/{dest_avatar_resource}/{dest_avatar.id}/avatar"
        dest_avatar_api_headers = {"PRIVATE-TOKEN": self.dest_client.private_token} if self.dest_client.private_token is not None else {}

        # Up-to-date if no source avatar url or implicit avatar url ('/path/to/proj/-/avatar')
        if src_avatar_url is None or src_avatar_url == f"{src_web_url}/-/avatar":
            # image up-to-date
            print(
                f"    - avatar image: {AnsiColors.HGRAY}up-to-date{AnsiColors.RESET} ({src_avatar_url} / {dest_avatar_url})"
            )
            return
        
        # Compare avatar size if destination avatar exist and not forced by user
        if dest_avatar_url is not None and not self.force_update_avatar:
            # fetch avatar size
            try:
                src_avatar_response = requests.head(url=src_avatar_api, headers=src_avatar_api_headers, verify=self.ssl_verify)
                _, src_avatar_size = src_avatar_response.raise_for_status(), src_avatar_response.headers['Content-Length']
                dest_avatar_response = requests.head(url=dest_avatar_api, headers=dest_avatar_api_headers, verify=self.ssl_verify)
                _, dest_avatar_size = dest_avatar_response.raise_for_status(), dest_avatar_response.headers['Content-Length']
                # compare avatar size
                if src_avatar_size == dest_avatar_size:
                    # image up-to-date
                    print(
                        f"    - avatar image: {AnsiColors.HGRAY}up-to-date{AnsiColors.RESET} ({src_avatar_url} / {dest_avatar_url}) size: {src_avatar_size}"
                    )
                    return
            except requests.RequestException as ge:
                print(
                    f"    - avatar image: compare {AnsiColors.HYELLOW}failed{AnsiColors.RESET} ({src_avatar_url} / {dest_avatar_url}) - assume update needed:",
                    ge,
                )
                # don't rethrow - continue anyway
                self.handle_warn(ge)

        # Update needed but dry run
        if self.dry_run:
            print(
                f"    - avatar image: {AnsiColors.HYELLOW}update needed{AnsiColors.RESET} ({src_avatar_url} / {dest_avatar_url})"
            )
            return
        
        # Update avatar
        try:
            dest_avatar.avatar = requests.get(url=src_avatar_api, headers=src_avatar_api_headers, verify=self.ssl_verify).content
            dest_avatar.save()
            print(
                f"    - avatar image: {AnsiColors.HYELLOW}updated{AnsiColors.RESET} ({src_avatar_url})"
            )
        except GitlabUpdateError as ge:
            print(
                f"    - avatar image: update {AnsiColors.HYELLOW}failed{AnsiColors.RESET} ({src_avatar_url})",
                ge,
            )
            self.handle_warn(ge)
        return        

    # Synchronizes a GitLab project
    # $1: source project JSON
    # $2: destination parent group ID (number)
    def sync_project(
        self, src_project: Project, dest_parent_group: Optional[Group]
    ) -> None:
        dest_project_path = self.to_dest_path(src_project.path_with_namespace)
        print(
            f"  - 🏠 Project {AnsiColors.BLUE}{src_project.path_with_namespace}{AnsiColors.RESET} ({src_project.get_id()}) => {AnsiColors.BLUE}{dest_project_path}{AnsiColors.RESET}..."
        )
        self.projects_count += 1
        # dump project json (for debug)
        # echo "$src_project" > "project-$src_project_id.json"

        dest_project: Optional[Project] = None

        # 1: sync project
        dest_visibility = self.adjust_visibility(
            src_project.attributes.get("visibility", GlVisibility.public)
        )
        try:
            dest_project = self.dest_client.projects.get(dest_project_path)
            changed_attr = []
            if dest_project.name != src_project.name:
                dest_project.name = src_project.name
                changed_attr.append("name")
            if dest_project.visibility != dest_visibility:
                if self.skip_visibility:
                    print(
                        f"    - visibility: {AnsiColors.HYELLOW}skipping update{AnsiColors.RESET} ({dest_project.visibility} -> {dest_visibility}) ({dest_project.get_id()})"
                    )
                else:
                    dest_project.visibility = dest_visibility
                    changed_attr.append("visibility")
            if (
                not self.project_description
                and dest_project.description != src_project.description
            ):
                dest_project.description = src_project.description
                changed_attr.append("description")
            if not changed_attr:
                print(
                    f"    - properties: {AnsiColors.HGRAY}up-to-date{AnsiColors.RESET} ({dest_project.get_id()})"
                )
            elif self.dry_run:
                print(
                    f"    - properties: {AnsiColors.HYELLOW}update needed{AnsiColors.RESET} ({', '.join(changed_attr)}) ({dest_project.get_id()})"
                )
            else:
                try:
                    dest_project.save()
                    print(
                        f"    - properties: {AnsiColors.HYELLOW}updated{AnsiColors.RESET} ({', '.join(changed_attr)}) ({dest_project.get_id()})"
                    )
                except GitlabUpdateError as ge:
                    print(
                        f"    - properties: update {AnsiColors.HRED}failed{AnsiColors.RESET} ({', '.join(changed_attr)}) ({dest_project.get_id()})",
                        ge,
                    )
                    self.handle_error(ge)

        except GitlabGetError as ge:
            if ge.response_code != 404:
                print(
                    f"    - project: get {AnsiColors.HRED}failed{AnsiColors.RESET} ({dest_project_path})",
                    ge,
                )
                self.handle_error(ge)
                return
            # dest project does not exist: create (disable MR and issues as they are cloned projects)
            # TODO enable project as a ci catalog resource
            # It seems that the option isn't present in the REST API for now
            if self.dry_run:
                print(
                    f"    - project: {AnsiColors.HGREEN}create needed{AnsiColors.RESET} ({dest_project_path})"
                )
                return
            else:
                try:
                    dest_project = self.dest_client.projects.create(
                        {
                            "path": src_project.path,
                            "name": src_project.name,
                            "visibility": dest_visibility,
                            "description": src_project.description,
                            # ??? "default_branch": src_default_branch,
                            "namespace_id": dest_parent_group.get_id(),
                            ** self.new_project_options,
                        }
                    )
                    print(
                        f"    - project: {AnsiColors.HGREEN}created{AnsiColors.RESET} ({dest_project.get_id()})"
                    )
                except GitlabCreateError as ge:
                    print(
                        f"    - project: create {AnsiColors.HRED}failed{AnsiColors.RESET} ({dest_project_path} in parent group {dest_parent_group.get_id()})",
                        ge,
                    )
                    self.handle_error(ge)
                    return

        # 2: sync Avatar
        self.sync_avatar(src_project, dest_project)

        # 3: sync Git repository
        self.sync_git_repo(src_project, dest_project)

        # 4: sync Releases
        self.sync_releases(src_project, dest_project)

    # Synchronizes recursively a GitLab group
    def sync_group(self, src_group: Group, dest_parent_group: Group) -> None:
        dest_group_full_path = self.to_dest_path(src_group.full_path)
        print(
            f"🏢 Group {AnsiColors.BLUE}{src_group.full_path}{AnsiColors.RESET} ({src_group.get_id()}) => {AnsiColors.BLUE}{dest_group_full_path}{AnsiColors.RESET}..."
        )
        self.groups_count += 1
        # dump group json (for debug)
        # echo "$src_group" > "group-$src_group_id.json"
        dest_group = None
        is_root_group = src_group.full_path == self.src_sync_path
        dest_visibility = self.adjust_visibility(src_group.visibility)

        # 1: sync group properties
        try:
            dest_group = self.dest_client.groups.get(dest_group_full_path)
            # dest group exists: sync
            changed_attr = []
            if dest_group.name != src_group.name:
                dest_group.name = src_group.name
                changed_attr.append("name")
            if dest_group.visibility != dest_visibility:
                if self.skip_visibility:
                    print(
                        f"- visibility: {AnsiColors.HYELLOW}skipping update{AnsiColors.RESET} ({dest_group.visibility} -> {dest_visibility}) ({dest_group.get_id()})"
                    )
                else:
                    dest_group.visibility = dest_visibility
                    changed_attr.append("visibility")
            if (
                self.group_description
                and dest_group.description != src_group.description
            ):
                dest_group.description = src_group.description
                changed_attr.append("description")
            if not changed_attr:
                print(
                    f"- properties: {AnsiColors.HGRAY}up-to-date{AnsiColors.RESET} ({dest_group.get_id()})"
                )
            elif self.dry_run:
                print(
                    f"- properties: {AnsiColors.HYELLOW}update needed{AnsiColors.RESET} ({', '.join(changed_attr)}) ({dest_group.get_id()})"
                )
            else:
                # update
                try:
                    dest_group.save()
                    print(
                        f"- properties: {AnsiColors.HYELLOW}updated{AnsiColors.RESET} ({', '.join(changed_attr)}) ({dest_group.get_id()})"
                    )
                except GitlabUpdateError as ge:
                    print(
                        f"- properties: update {AnsiColors.HRED}failed{AnsiColors.RESET} ({', '.join(changed_attr)}) ({dest_group_full_path})",
                        ge,
                    )
                    self.handle_error(ge)
        except GitlabGetError as ge:
            if ge.response_code != 404:
                print(
                    f"- group: get {AnsiColors.HRED}failed{AnsiColors.RESET} ({dest_group_full_path})",
                    ge,
                )
                self.handle_error(ge)
                return
            # dest group does not exist: create
            if self.dry_run:
                print(f"- group: {AnsiColors.HGREEN}create needed{AnsiColors.RESET}")
                return
            else:
                try:
                    dest_group = self.dest_client.groups.create(
                        {
                            "path": basename(dest_group_full_path),
                            "name": src_group.name,
                            "visibility": dest_visibility,
                            "description": src_group.description,
                            "parent_id": dest_parent_group.get_id(),
                            ** self.new_group_options,
                        }
                    )
                    print(
                        f"- group: {AnsiColors.HGREEN}created{AnsiColors.RESET} ({dest_group.get_id()})"
                    )
                except GitlabCreateError as ge:
                    print(
                        f"    - group: create {AnsiColors.HRED}failed{AnsiColors.RESET} ({dest_group_full_path})",
                        ge,
                    )
                    self.handle_error(ge)
                    return

        # create TBC_NAMESPACE variable in top group
        if is_root_group:
            try:
                var = dest_group.variables.get("TBC_NAMESPACE")
                print(
                    f"- TBC_NAMESPACE group variable: {AnsiColors.HGRAY}up-to-date{AnsiColors.RESET} ({var.value})"
                )
            except GitlabGetError as ge:
                if ge.response_code != 404:
                    print(
                        f"- TBC_NAMESPACE group variable: get {AnsiColors.HYELLOW}failed{AnsiColors.RESET}",
                        ge,
                    )
                    self.handle_warn(ge)
                    # continue anyway
                elif self.dry_run:
                    print(
                        f"- TBC_NAMESPACE group variable: {AnsiColors.HGREEN}create needed{AnsiColors.RESET} ({self.dest_sync_path})"
                    )
                else:
                    try:
                        dest_group.variables.create(
                            {"key": "TBC_NAMESPACE", "value": self.dest_sync_path}
                        )
                        print(
                            f"- TBC_NAMESPACE group variable: {AnsiColors.HGREEN}created{AnsiColors.RESET} ({self.dest_sync_path})"
                        )
                    except GitlabCreateError as ge:
                        print(
                            f"- TBC_NAMESPACE group variable: create {AnsiColors.HYELLOW}failed{AnsiColors.RESET}",
                            ge,
                        )
                        self.handle_warn(ge)
                        # continue anyway

        # 2: sync Avatar
        self.sync_avatar(src_group, dest_group)

        # 3: sync sub-projects
        subprojects = src_group.projects.list(all=True)
        print(f"- sync {len(subprojects)} sub projects...")
        for src_project in subprojects:
            if not self.is_included(src_project.path_with_namespace):
                print(
                    f"  - 🏠 Project {AnsiColors.BLUE}{src_project.path_with_namespace}{AnsiColors.RESET} does not match includes: {AnsiColors.HGRAY}skip{AnsiColors.RESET}"
                )
            elif self.is_excluded(src_project.path_with_namespace):
                print(
                    f"  - 🏠 Project {AnsiColors.BLUE}{src_project.path_with_namespace}{AnsiColors.RESET} matches excludes: {AnsiColors.HGRAY}skip{AnsiColors.RESET}"
                )
            else:
                self.sync_project(
                    self.src_client.projects.get(src_project.get_id()), dest_group
                )

        # 4: sync sub-groups
        subgroups = src_group.subgroups.list(all=True)
        print(f"- sync {len(subgroups)} sub groups...")
        for src_subgroup in subgroups:
            if not self.is_included(src_subgroup.full_path):
                print(
                    f"🏢 Group {AnsiColors.BLUE}{src_subgroup.full_path}{AnsiColors.RESET} does not match includes: {AnsiColors.HGRAY}skip{AnsiColors.RESET}"
                )
            elif self.is_excluded(src_subgroup.full_path):
                print(
                    f"🏢 Group {AnsiColors.BLUE}{src_subgroup.full_path}{AnsiColors.RESET} matches excludes: {AnsiColors.HGRAY}skip{AnsiColors.RESET}"
                )
            else:
                self.sync_group(
                    self.src_client.groups.get(src_subgroup.get_id()), dest_group
                )


def basename(path: Optional[str]) -> Optional[str]:
    return path.split("/")[-1] if path else None


def dirname(path: Optional[str]) -> Optional[str]:
    return "/".join(path.split("/")[0:-1]) if path else None


def to_url(api_url: str) -> str:
    return api_url[0:-7] if api_url.endswith("/api/v4") else api_url

def read_paths_list_from_file(file_path: Path) -> list[str]:
    # read a list of paths from a file (one per line), ignoring comments,
    # blank lines and leading/trailing spaces
    with open(file_path, "r") as f:
        lines = (line.partition('#')[0] for line in f)
        return [line.strip() for line in lines if line.strip()]

def simple_json_to_dict(json_str: str, source: Union[str,Path]) -> dict[str,Union[int,str]]:
    options = json.loads(json_str)
    assert (
        isinstance(options, (dict))
    ), f"Invalid type of JSON in {source}, expected an object"
    for k, v in options.items():
        assert (
            isinstance(v, (int, str))
        ), f"Invalid value type for key {k} in {source}, expected only strings, integers and booleans"
    return options

def trueish_env_var(env_var_name: str) -> bool:
    value = os.getenv(env_var_name)
    return bool(value) and (value.lower() not in [ "0", "false", "no" ])

def run() -> None:
    # define command parser
    parser = argparse.ArgumentParser(
        prog="gitlab-cp",
        description="This tool recursively copies/synchronizes a GitLab group from one GitLab server to another.",
    )
    parser.add_argument(
        "--src-api",
        default=os.getenv("SRC_GITLAB_API"),
        help="GitLab source API",
    )
    parser.add_argument(
        "--src-token",
        default=os.getenv("SRC_TOKEN"),
        help="GitLab source token (optional if source GitLab group and sub projects have `public` visibility)",
    )
    parser.add_argument(
        "--src-sync-path",
        default=os.getenv("SRC_SYNC_PATH"),
        help="GitLab source root group path to synchronize",
    )
    parser.add_argument(
        "--dest-api",
        default=os.getenv("DEST_GITLAB_API"),
        help="GitLab destination API",
    )
    parser.add_argument(
        "--dest-token",
        default=os.getenv("DEST_TOKEN"),
        help="GitLab destination token with at least scopes `api,read_repository,write_repository` and `Owner` role",
    )
    parser.add_argument(
        "--dest-sync-path",
        default=os.getenv("DEST_SYNC_PATH"),
        help="GitLab destination root group path to synchronize (defaults to --src-sync-path)",
    )
    parser.add_argument(
        "--max-visibility",
        default=os.getenv("MAX_VISIBILITY") or GlVisibility.public,
        type=GlVisibility,
        choices=list(GlVisibility),
        help="maximum visibility of groups and projects in destination group",
    )
    parser.add_argument(
        "--skip-visibility",
        action="store_true",
        default=trueish_env_var("SKIP_VISIBILITY"),
        help="skip updating the destination group or project visibility (when it exists already)",
    )
    parser.add_argument(
        "--exclude",
        action="append",
        default=os.getenv("EXCLUDE").split(",") if os.getenv("EXCLUDE") else [],
        help="project/group path to exclude from processing (relative to --src-sync-path)",
    )
    parser.add_argument(
        "--exclude-from",
        default=os.getenv("EXCLUDE_FROM"),
        type=Path,
        help="a file which lists paths to exclude (one per line); incompatible with --exclude",
    )
    parser.add_argument(
        "--include",
        action="append",
        default=os.getenv("INCLUDE").split(",") if os.getenv("INCLUDE") else [],
        help="project/group path to include for processing (relative to --src-sync-path); all paths are included by default",
    )
    parser.add_argument(
        "--include-from",
        default=os.getenv("INCLUDE_FROM"),
        type=Path,
        help="a file which lists paths to include (one per line); incompatible with --include",
    )
    parser.add_argument(
        "--insecure",
        action="store_true",
        default=trueish_env_var("INSECURE"),
        help="skip SSL verification",
    )
    parser.add_argument(
        "--update-release",
        default=trueish_env_var("UPDATE_RELEASE"),
        action="store_true",
        help="force the update of the latest release",
    )
    parser.add_argument(
        "--update-avatar",
        default=trueish_env_var("UPDATE_AVATAR"),
        action="store_true",
        help="force update the avatar images even when they exist and look the same",
    )
    parser.add_argument(
        "--no-group-description",
        default=trueish_env_var("GROUP_DESCRIPTION_DISABLED"),
        action="store_true",
        help="don't synchronize group description",
    )
    parser.add_argument(
        "--no-project-description",
        default=trueish_env_var("PROJECT_DESCRIPTION_DISABLED"),
        action="store_true",
        help="don't synchronize project description",
    )
    parser.add_argument(
        "--new-group-options",
        default=os.getenv("NEW_GROUP_OPTIONS"),
        help="a JSON string with extra options for groups creation",
    )
    parser.add_argument(
        "--new-group-options-from",
        default=os.getenv("NEW_GROUP_OPTIONS_FROM"),
        type=Path,
        help="a JSON file with extra options for groups creation; incompatible with --new-group-options",
    )
    parser.add_argument(
        "--new-project-options",
        default=os.getenv("NEW_PROJECT_OPTIONS"),
        help="a JSON string with extra options for projects creation",
    )
    parser.add_argument(
        "--new-project-options-from",
        default=os.getenv("NEW_PROJECT_OPTIONS_FROM"),
        type=Path,
        help="a JSON file with extra options for projects creation; incompatible with --new-project-options",
    )
    parser.add_argument(
        "--dry-run",
        default=trueish_env_var("DRY_RUN"),
        action="store_true",
        help="dry run (don't execute any write action)",
    )
    parser.add_argument(
        "--halt-on-error",
        default=trueish_env_var("HALT_ON_ERROR"),
        action="store_true",
        help="halt synchronizing whenever an error occurs",
    )
    parser.add_argument(
        "--cache-dir",
        default=os.getenv("CACHE_DIR", ".work"),
        help="cache directory (used to download resources such as images and Git repositories)",
    )

    # parse command and args
    args = parser.parse_args()

    assert (
        args.src_api
    ), "Source API is required (use --src-api CLI option or SRC_GITLAB_API variable)"
    assert args.src_sync_path, "Source sync path is required (use --src-sync-path CLI option or SRC_SYNC_PATH variable)"
    assert (
        args.dest_api
    ), "Dest API is required (use --dest-api CLI option or DEST_GITLAB_API variable)"
    dest_sync_path = args.dest_sync_path or args.src_sync_path
    assert dest_sync_path, "Dest sync path is required (use --dest-sync-path CLI option or DEST_SYNC_PATH variable)"
    assert (
        args.src_api != args.dest_api or args.src_sync_path != dest_sync_path
    ), "Cannot have same source and destination"
    assert (
        args.dest_token
    ), "Dest token is required (use --dest-token CLI option or DEST_TOKEN variable)"
    assert (
        not args.exclude or not args.exclude_from
    ), "Cannot use both --exclude and --exclude-from"
    assert (
        not args.exclude_from or args.exclude_from.is_file()
    ), f"No such file: {args.exclude_from}"
    assert (
        not args.include or not args.include_from
    ), "Cannot use both --include and --include-from"
    assert (
        not args.include_from or args.include_from.is_file()
    ), f"No such file: {args.include_from}"
    assert (
        not args.new_group_options or not args.new_group_options_from
    ), "Cannot use both --new-group-options and --new-group-options-from"
    assert (
        not args.new_group_options_from or args.new_group_options_from.is_file()
    ), f"No such file: {args.new_group_options_from}"
    assert (
        not args.new_project_options or not args.new_project_options_from
    ), "Cannot use both --new-project-options and --new-project-options-from"
    assert (
        not args.new_project_options_from or args.new_project_options_from.is_file()
    ), f"No such file: {args.new_project_options_from}"

    new_group_options: dict[str, Union[int,str]] = {}
    if args.new_group_options:
        new_group_options = simple_json_to_dict(args.new_group_options, "--new-group-options")
    elif args.new_group_options_from:
        new_group_options = simple_json_to_dict(args.new_group_options_from.read_text(), str(args.new_group_options_from))

    # default options for new project: disable issues and MR
    new_project_options: dict[str, Union[int,str]] = { "issues_access_level": "disabled", "merge_requests_access_level": "disabled" }
    if args.new_project_options:
        new_project_options = simple_json_to_dict(args.new_project_options, "--new-project-options")
    elif args.new_project_options_from:
        new_project_options = simple_json_to_dict(args.new_project_options_from.read_text(), args.new_project_options_from)

    exclude_list = []
    if args.exclude:
        exclude_list = args.exclude
    elif args.exclude_from:
        exclude_list = read_paths_list_from_file(args.exclude_from)

    include_list = []
    if args.include:
        include_list = args.include
    elif args.include_from:
        include_list = read_paths_list_from_file(args.include_from)

    print("Synchronizing GitLab group")
    print(
        f"- source API  (--src-api)        : {AnsiColors.CYAN}{args.src_api}{AnsiColors.RESET}"
    )
    print(
        f"- src path    (--src-sync-path)  : {AnsiColors.CYAN}{args.src_sync_path}{AnsiColors.RESET}"
    )
    print(
        f"- dest API    (--dest-api)       : {AnsiColors.CYAN}{args.dest_api}{AnsiColors.RESET}"
    )
    print(
        f"- dest path   (--dest-sync-path) : {AnsiColors.CYAN}{dest_sync_path}{AnsiColors.RESET}"
    )
    print(
        f"- max visi.   (--max-visibility) : {AnsiColors.CYAN}{args.max_visibility}{AnsiColors.RESET}"
    )
    print(
        f"- skip visi  (--skip-visibility) : {AnsiColors.CYAN}{args.skip_visibility}{AnsiColors.RESET}"
    )
    print(
        f"- exclude     (--exclude(-from)) : {AnsiColors.CYAN}{', '.join(exclude_list)}{AnsiColors.RESET}"
    )
    print(
        f"- include     (--include(-from)) : {AnsiColors.CYAN}{', '.join(include_list)}{AnsiColors.RESET}"
    )
    print(
        f"- insecure    (--insecure)       : {AnsiColors.CYAN}{args.insecure}{AnsiColors.RESET}"
    )
    print(
        f"- upd release (--update-release) : {AnsiColors.CYAN}{args.update_release}{AnsiColors.RESET}"
    )
    print(
        f"- upd. avatar (--update-avatar)  : {AnsiColors.CYAN}{args.update_avatar}{AnsiColors.RESET}"
    )
    print(
        f"- dry run     (--dry-run)        : {AnsiColors.CYAN}{args.dry_run}{AnsiColors.RESET}"
    )
    print(
        f"- halt on err. (--halt-on-error) : {AnsiColors.CYAN}{args.halt_on_error}{AnsiColors.RESET}"
    )
    print(
        f"- cache dir   (--cache-dir)      : {AnsiColors.CYAN}{args.cache_dir}{AnsiColors.RESET}"
    )
    print(
        f"- new group options (--new-group-options)       : {AnsiColors.CYAN}{json.dumps(new_group_options)}{AnsiColors.RESET}"
    )
    print(
        f"- new project options (--new-project-options)   : {AnsiColors.CYAN}{json.dumps(new_project_options)}{AnsiColors.RESET}"
    )
    print(
        f"- skip group desc.   (--no-group-description)   : {AnsiColors.CYAN}{args.no_group_description}{AnsiColors.RESET}"
    )
    print(
        f"- skip project desc. (--no-project-description) : {AnsiColors.CYAN}{args.no_project_description}{AnsiColors.RESET}"
    )
    print()

    # TODO: configurable
    work_dir = Path(args.cache_dir)
    work_dir.mkdir(exist_ok=True)

    client = Synchronizer(
        Gitlab(
            url=to_url(args.src_api),
            private_token=args.src_token,
            ssl_verify=not args.insecure,
            per_page=100,
        ),
        args.src_sync_path,
        (
            Gitlab(
                url=to_url(args.dest_api),
                private_token=args.dest_token,
                ssl_verify=not args.insecure,
                per_page=100,
            )
            if args.dest_api
            else None
        ),
        dest_sync_path,
        work_dir,
        max_visibility=args.max_visibility,
        skip_visibility=args.skip_visibility,
        exclude=exclude_list,
        include=include_list,
        update_release=args.update_release,
        group_description=(not args.no_group_description),
        project_description=(not args.no_project_description),
        dry_run=args.dry_run,
        continue_on_error=not args.halt_on_error,
        update_avatar=args.update_avatar,
        ssl_verify=not args.insecure,
        new_group_options=new_group_options,
        new_project_options=new_project_options,
    )
    # retrieve src root group
    try:
        src_group = client.src_client.groups.get(client.src_sync_path)
    except GitlabGetError as ge:
        print(
            f"Src group {AnsiColors.BLUE}{client.src_sync_path}{AnsiColors.RESET}: get {AnsiColors.HRED}failed{AnsiColors.RESET}",
            ge,
        )
        raise ge

    # retrieve dest parent group
    try:
        dest_parent_group_path = dirname(dest_sync_path)
        dest_parent_group = (
            client.dest_client.groups.get(dest_parent_group_path)
            if dest_parent_group_path
            else None
        )
    except GitlabGetError as ge:
        print(
            f"Dest parent group {AnsiColors.BLUE}{dirname(dest_sync_path)}{AnsiColors.RESET}: get {AnsiColors.HRED}failed{AnsiColors.RESET}",
            ge,
        )
        raise ge

    # synchronize group (recurse)
    client.sync_group(src_group, dest_parent_group)

    print("Done!")
    print(
        "----------------------------------------------------------------------------------------------"
    )
    print(
        f"Summary: {client.groups_count} groups and {client.projects_count} projects synchronized; {len(client.warnings)} warnings; {len(client.errors)} errors (see logs)"
    )

    if len(client.errors) > 0:
        exit(128)
