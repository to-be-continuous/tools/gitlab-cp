## [1.3.4](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.3.3...1.3.4) (2025-01-03)


### Bug Fixes

* **deps:** update poetry dependencies ([c6a4bee](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/c6a4bee18dacb6d489f6b747ce6fc94b31c18098))

## [1.3.3](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.3.2...1.3.3) (2024-12-30)


### Bug Fixes

* **avatar:** get avatar using src header (not dest) ([a376a5d](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/a376a5dbb9d3971a5c7485cad3fdd9b7f5596550))

## [1.3.2](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.3.1...1.3.2) (2024-12-06)


### Bug Fixes

* update lock file ([31e9e9a](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/31e9e9a59eab175e75ed2a02ea84da05c9944948))

## [1.3.1](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.3.0...1.3.1) (2024-12-06)


### Bug Fixes

* **avatar:** avatar comparison - check HEAD requests return status ([685671f](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/685671f3415a1ee5dceb1ae25e6789a0b5b3c63e))

# [1.3.0](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.2.4...1.3.0) (2024-12-05)


### Bug Fixes

* skip Git sync on empty repositories ([46d11b1](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/46d11b1af35becf345921ecfa4a752f8f9acc8de))


### Features

* **avatar:** use GitLab API to download avatar on private repositories ([edc67c1](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/edc67c10108cd83e553e2932954a22c41558118a))

## [1.2.4](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.2.3...1.2.4) (2024-10-11)


### Bug Fixes

* **deps:** update poetry dependencies ([264dc58](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/264dc58815e59467f530dbc1f8478fb75c89d262))

## [1.2.3](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.2.2...1.2.3) (2024-10-04)


### Bug Fixes

* **deps:** update dependency python-gitlab to v4.12.2 ([f6ee8e0](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/f6ee8e084c1a26c6f37b7a89f949873bda5a3542))

## [1.2.2](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.2.1...1.2.2) (2024-08-09)


### Bug Fixes

* **deps:** update poetry dependencies ([5890f2e](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/5890f2e1dc057189e7015839bd6c831c984fa5ca))

## [1.2.1](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.2.0...1.2.1) (2024-07-19)


### Bug Fixes

* **deps:** update poetry dependencies ([bb319ef](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/bb319ef6fb3175fde65379c04548ecfe7b2ea09c))

# [1.2.0](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.1.8...1.2.0) (2024-07-12)


### Features

* add --exclude-from and --include-from options (flat file alternatives to --exclude/--include) ([06dd1c1](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/06dd1c13445d678788daf30dae1c7f3d25057d93))
* add --include option (sync only some projects and subgroups) ([3266e81](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/3266e8127bac2d809a0b856f80cec4b8fbde01fa))
* add --new-{group,project}-option(-from) options (JSON options for group/project creation) ([105fcee](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/105fcee3b2feeff4f157a16e5133063d5bf4577c))
* add --skip-visibility option (preserve existing groups/projects visibility) ([f4a57e8](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/f4a57e8e5131e2490329c3dbb4bf902a10dafa26))
* handle $DRY_RUN and $HALT_ON_ERROR env vars ([23d38cb](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/23d38cb5925fda8774ffd7585eae30aeefc616d5))
* improve boolean options from env vars (empty/"[secure]" is not true) ([2b0b789](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/2b0b7891b4115d3b7fad77ead9012bdbba135e24))

## [1.1.8](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.1.7...1.1.8) (2024-06-30)


### Bug Fixes

* fix insecure on avatar by replace urlretrieve by requests.get ([610c1a7](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/610c1a79f3f597b2cd8cf9c342edcb10d8fa8c02)), closes [#3](https://gitlab.com/to-be-continuous/tools/gitlab-cp/issues/3)

## [1.1.7](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.1.6...1.1.7) (2024-05-31)


### Bug Fixes

* **deps:** update poetry dependencies ([43dbee2](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/43dbee2da3ef296ae72a03d355ed0ba7a892562f))

## [1.1.6](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.1.5...1.1.6) (2024-05-27)


### Bug Fixes

* don't fail when Git repo has no commit ([8ae334d](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/8ae334d65e7d5ab07be6b84843262aeef69318d1))
* use subgroups API instead of descendant_groups ([ac5fad3](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/ac5fad33a36e1e39917ffb75310c5c629442463c)), closes [#2](https://gitlab.com/to-be-continuous/tools/gitlab-cp/issues/2)

## [1.1.5](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.1.4...1.1.5) (2024-04-30)


### Bug Fixes

* add more logs in case of ppty update failure ([cadb6dd](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/cadb6dd736cb8a0e8f825d9788c921c62f6a22dc))
* don't update avatar if src group/project is not public ([7140a4a](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/7140a4a1c272bb27ebd61ce7b98dce4aa589b5df))

## [1.1.4](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.1.3...1.1.4) (2024-04-30)


### Bug Fixes

* wrong and operator ([2660fbc](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/2660fbc0f3f0db0cd3105c2409acb586cf2b2e2b))

## [1.1.3](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.1.2...1.1.3) (2024-04-30)


### Bug Fixes

* parent group is None for a 1-depth path ([53c1558](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/53c1558d1666d11fb2a3608e00a19fc459464474))

## [1.1.2](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.1.1...1.1.2) (2024-04-29)


### Bug Fixes

* default exclude is empty list ([08e8a92](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/08e8a92c0223b1e9cbc9ea8deea98febfd7fecc6))

## [1.1.1](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.1.0...1.1.1) (2024-04-29)


### Bug Fixes

* add Git tool to docker image ([09b8435](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/09b843530ee3af14ae4ab1876f1d1c502e2f245c))

# [1.1.0](https://gitlab.com/to-be-continuous/tools/gitlab-cp/compare/1.0.0...1.1.0) (2024-04-27)


### Features

* add cache dir argument ([d3d6c0a](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/d3d6c0a32c681212a8441285400205f99b6ce2a1))
* force update release only on latest ([69a6660](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/69a6660524b1486908f98ef9b44ae8ddcb1cfce8))
* reliable dry-run ([a30228c](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/a30228ca74aeaa3cef9c18e73dcce15b4f91ebfc))

# 1.0.0 (2024-04-07)


### Features

* initial commit ([8daeca6](https://gitlab.com/to-be-continuous/tools/gitlab-cp/commit/8daeca679cf543308fe4e3d4bdcace4af593d06f))
