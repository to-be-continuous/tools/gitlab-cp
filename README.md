# GitLab Copy CLI

This project provides a CLI tool able to recursively copy/synchronize a GitLab group from one GitLab server to another.

## Install

`gitlab-cp` requires Python 3.11 or higher and can be installed with the following command:

```bash
pip install gitlab-cp --index-url https://gitlab.com/api/v4/projects/to-be-continuous%2Ftools%2Fgitlab-cp/packages/pypi/simple --upgrade

# obtain help
gitlab-cp --help
```

## Usage

```bash
usage: gitlab-cp [-h] [--src-api SRC_API] [--src-token SRC_TOKEN] [--src-sync-path SRC_SYNC_PATH] [--dest-api DEST_API] [--dest-token DEST_TOKEN] [--dest-sync-path DEST_SYNC_PATH] [--max-visibility {public,internal,private}]
                 [--skip-visibility] [--exclude EXCLUDE] [--exclude-from EXCLUDE_FROM] [--include INCLUDE] [--include-from INCLUDE_FROM] [--insecure] [--update-release] [--update-avatar] [--no-group-description]
                 [--no-project-description] [--new-group-options NEW_GROUP_OPTIONS] [--new-group-options-from NEW_GROUP_OPTIONS_FROM] [--new-project-options NEW_PROJECT_OPTIONS]
                 [--new-project-options-from NEW_PROJECT_OPTIONS_FROM] [--dry-run] [--halt-on-error] [--cache-dir CACHE_DIR]

This tool recursively copies/synchronizes a GitLab group from one GitLab server to another.

options:
  -h, --help            show this help message and exit
  --src-api SRC_API     GitLab source API
  --src-token SRC_TOKEN
                        GitLab source token (optional if source GitLab group and sub projects have `public` visibility)
  --src-sync-path SRC_SYNC_PATH
                        GitLab source root group path to synchronize
  --dest-api DEST_API   GitLab destination API
  --dest-token DEST_TOKEN
                        GitLab destination token with at least scopes `api,read_repository,write_repository` and `Owner` role
  --dest-sync-path DEST_SYNC_PATH
                        GitLab destination root group path to synchronize (defaults to --src-sync-path)
  --max-visibility {public,internal,private}
                        maximum visibility of groups and projects in destination group
  --skip-visibility     skip updating the destination group or project visibility (when it exists already)
  --exclude EXCLUDE     project/group path to exclude from processing (relative to --src-sync-path)
  --exclude-from EXCLUDE_FROM
                        a file which lists paths to exclude (one per line); incompatible with --exclude
  --include INCLUDE     project/group path to include for processing (relative to --src-sync-path); all paths are included by default
  --include-from INCLUDE_FROM
                        a file which lists paths to include (one per line); incompatible with --include
  --insecure            skip SSL verification
  --update-release      force the update of the latest release
  --update-avatar       force update the avatar images even when they exist and look the same
  --no-group-description
                        don't synchronize group description
  --no-project-description
                        don't synchronize project description
  --new-group-options NEW_GROUP_OPTIONS
                        a JSON string with extra options for groups creation
  --new-group-options-from NEW_GROUP_OPTIONS_FROM
                        a JSON file with extra options for groups creation; incompatible with --new-group-options
  --new-project-options NEW_PROJECT_OPTIONS
                        a JSON string with extra options for projects creation
  --new-project-options-from NEW_PROJECT_OPTIONS_FROM
                        a JSON file with extra options for projects creation; incompatible with --new-project-options
  --dry-run             dry run (don't execute any write action)
  --halt-on-error       halt synchronizing whenever an error occurs
  --cache-dir CACHE_DIR
                        cache directory (used to download resources such as images and Git repositories)
```

| CLI option                   | Env. Variable                   | Description                                                                                                           |
| ---------------------------- | ------------------------------- | --------------------------------------------------------------------------------------------------------------------- |
| `--src-api`                  | `$SRC_GITLAB_API`               | GitLab source API url (**mandatory**)                                                                                 |
| `--src-token`                | `$SRC_TOKEN`                    | GitLab source token (_optional_ if source GitLab group and sub projects have `public` visibility)                     |
| `--src-sync-path`            | `$SRC_SYNC_PATH`                | GitLab source root group path to synchronize (**mandatory**)                                                          |
| `--dest-api`                 | `$DEST_GITLAB_API`              | GitLab destination API url (**mandatory**)                                                                            |
| `--dest-token`               | `$DEST_TOKEN`                   | GitLab destination token with at least scopes `api,read_repository,write_repository` and `Owner` role (**mandatory**) |
| `--dest-sync-path`           | `$DEST_SYNC_PATH`               | GitLab destination root group path to synchronize (defaults to `--src-sync-path`)                                     |
| `--max-visibility`           | `$MAX_VISIBILITY`               | maximum visibility of projects in destination group (defaults to `public`)                                            |
| `--skip-visibility`          | `$SKIP_VISIBILITY`              | skip updating the destination group or project visibility (when it exists already)                                    |
| `--exclude`                  | `$EXCLUDE`                      | project/group path(s) to exclude (multiple CLI option; env. variable is a coma separated list; wins over `--include`) |
| `--exclude-from`             | `$EXCLUDE_FROM`                 | a file which lists paths to exclude (one per line); incompatible with `--exclude` / `$EXCLUDE`                        |
| `--include`                  | `$INCLUDE`                      | project/group path(s) to include (multiple CLI option; env. var. is coma separated; all paths included by default)    |
| `--include-from`             | `$INCLUDE_FROM`                 | a file which lists paths to include (one per line); incompatible with `--include` / `$INCLUDE`                        |
| `--insecure`                 | `$INSECURE`                     | skip SSL verification                                                                                                 |
| `--update-release`           | `$UPDATE_RELEASE`               | set to force the update of the latest release (in order to trigger GitLab CI/CD catalog publication)                  |
| `--update-avatar`            | `$UPDATE_AVATAR`                | force update the avatar images even when they exist and look the same                                                 |
| `--no-group-description`     | `$GROUP_DESCRIPTION_DISABLED`   | don't synchronize group description                                                                                   |
| `--no-project-description`   | `$PROJECT_DESCRIPTION_DISABLED` | don't synchronize project description                                                                                 |
| `--new-group-options`        | `$NEW_GROUP_OPTIONS`            | a JSON string with [extra options for groups creation](https://docs.gitlab.com/api/groups/#create-a-group) (no default value)                                               |
| `--new-group-options-from`   | `$NEW_GROUP_OPTIONS_FROM`       | a JSON file with [extra options for groups creation](https://docs.gitlab.com/api/groups/#create-a-group); incompatible with `--new-group-options`                             |
| `--new-project-options`      | `$NEW_PROJECT_OPTIONS`          | a JSON string with [extra options for projects creation](https://docs.gitlab.com/api/projects/#create-a-project) (default value disables issues and MR, *see below*)            |
| `--new-project-options-from` | `$NEW_PROJECT_OPTIONS_FROM`     | a JSON file with [extra options for projects creation](https://docs.gitlab.com/api/projects/#create-a-project); incompatible with `--new-project-options`                         |
| `--dry-run`                  | `$DRY_RUN`                      | dry run (don't execute any write action)                                                                              |
| `--halt-on-error`            | `$HALT_ON_ERROR`                | halt synchronizing when an error occurs                                                                               |
| `--cache-dir`                | `$CACHE_DIR`                    | cache directory (used to download resources such as images and Git repositories) (defaults to `.work`)                |

Projects are copied with the following default options:

```json
{
  "issues_access_level": "disabled",
  "merge_requests_access_level": "disabled"
}
```

You can customize those default options with your own ones with the `--new-project-options` / `--new-project-options-from` CLI options.

Similarly, you can customize default group options with the `--new-group-options` / `--new-group-options-from` CLI options.

## Developers

`gitlab-cp` is implemented in Python and relies on [Poetry](https://python-poetry.org/) for its packaging and dependency management.

```bash
# install dependencies
poetry install

# run from code
poetry run gitlab-cp \
  --src-api https://gitlab.com/api/v4 \
  --src-sync-path to-be-continuous \
  --dest-api https://my.gitlab.host/api/v4 \
  --exclude samples \
  --exclude custom \
  --dest-token $MY_GITLAB_TOKEN
```
